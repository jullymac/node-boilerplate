const dotenv = require('dotenv').config().parsed;
const request = require("request");
const base_url = dotenv.APP_URL;

describe("Users", function() {

    describe("GET /users", function() {
        it("returns status code 200", function() {
            request.get(base_url + '/users', function(error, response, body) {
                expect(response.statusCode).toBe(200);
                done();
            });
        });
        /*
        it("returns Hello World", function(done) {
            request.get(base_url + '/users', function(error, response, body) {
                expect(body).toBe("Hello World");
                done();
            });
        });
        */
    });

});