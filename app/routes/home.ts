import { HomeController } from "../controllers/home";

export class HomeRoutes {
    static routes(app): void {

        let controller = new HomeController();

        app.route('/')
            .get(controller.list);
    }
}