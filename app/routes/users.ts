import { UsersController } from "../controllers/users";

export class UsersRoutes {

    static routes(app): void {

       let controller = new UsersController();

        app.route('/users')
            .get(controller.list);
    }
}