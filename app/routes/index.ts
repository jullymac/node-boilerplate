import * as express from "express";
import { HomeRoutes } from "./home";
import { UsersRoutes } from "./users";

export class Routes {
    public app: express.Application;

    public init( app ): void {
        this.app = app;
        this.routes();
    }

    private routes(){
        HomeRoutes.routes(this.app);
        UsersRoutes.routes(this.app);
    }

}