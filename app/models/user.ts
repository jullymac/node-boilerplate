import * as mongoose from 'mongoose';

const Schema = mongoose.Schema;

export const UserSchema = new Schema({
    uid: {
        type: String,
        required: 'Enter a UID code'
    },
    login: {
        type: String,
        required: 'Enter a login'
    },
    nome: {
        type: String,
        required: 'Enter a first name'
    }
});