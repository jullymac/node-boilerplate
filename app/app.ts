import * as express from "express";
import * as path from "path";
//import * as cookieParser from "cookie-parser";
import * as mongoose from "mongoose";
import * as createError from "http-errors";
//import * as logger from "morgan";
import { Routes } from "./routes/index";

class App {

    public app: express.Application;
    public routes: Routes = new Routes();

    constructor() {
        this.app = express();
        this.config();
    }

    private config(): void{

        // Set uso do pug nas views
        this.app.set('views', path.join(__dirname, 'views'));
        this.app.set('view engine', 'pug');

        // Suporte a application/json
        this.app.use(express.json());

        // Suporte a application/x-www-form-urlencoded
        this.app.use(express.urlencoded({ extended: false }));

        // Caminho da pasta de arquivos estáticos
        // - Se tiver rodando sem compilar, o caminho para o diretório é diferente
        let public_path = __dirname.includes('dist') ? path.join(__dirname, 'public') : path.join(__dirname, '..', 'dist', 'public');
        this.app.use(express.static(public_path));

        //
        //this.app.use(cookieParser());

        //
        //this.app.use(logger('dev'));

        // Setup mongo
        this.mongoSetup();

        // Setup routes
        this.routes.init(this.app);

        // Errors setup
        this.errors();
    }

    private errors(): void{
        // Pega erros 404 e direciona para o error handler
        this.app.use(function(req, res, next) {
            next(createError(404));
        });

        // error handler
        this.app.use(function(err, req, res, next) {

            // set locals, only providing error in development
            res.locals.message = err.message;
            res.locals.error = process.env.APP_ENV === 'development' ? err : {};

            // Set error code
            res.status(err.status || 500);

            // Render error page
            res.render('error');

        });
    }

    private mongoSetup(): void{
        mongoose.Promise = global.Promise;
        mongoose.connect(
            'mongodb://' + process.env.DB_USER + ':' + process.env.DB_PASS + '@' + process.env.DB_HOST + '/' + process.env.DB_DATABASE,
            { useNewUrlParser: true }
        );
    }

}

export default new App().app;