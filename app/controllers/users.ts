import * as mongoose from 'mongoose';

import { Request, Response } from 'express';
import { UserSchema } from "../models/user";

const User = mongoose.model('usuarios', UserSchema);

export class UsersController {

    public list (req: Request, res: Response) {
        User.find({}, (err, contact) => {
            if(err){
                res.send(err);
            }
            res.json(contact);
        });
    }

}