
import { Request, Response } from 'express';

export class HomeController {

    public list (req: Request, res: Response) {

        res.render('home', {
            title: 'Node Starter Structure',
            items: [
                { title: 'Express', url: 'http://expressjs.com' },
                { title: 'Typescript', url: 'https://www.typescriptlang.org' },
                { title: 'Webpack', url: 'https://webpack.js.org/' },
                { title: 'Pug', url: 'https://pugjs.org' },
                { title: 'SCSS', url: 'https://sass-lang.com/' }
            ]
        });
    }

}