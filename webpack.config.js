const path = require('path');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const OptimizeCSSAssetsPlugin = require("optimize-css-assets-webpack-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");

module.exports = env => {

    const isDevMode = env.APP_ENV !== 'production';
    const publicPath = path.resolve(__dirname, 'dist/public');

    return {
        mode: isDevMode ? 'development' : 'production',
        entry: './app/assets/entry.js',
        output: {
            filename: 'main.js',
            path: publicPath
        },
        devtool: isDevMode ? "source-map" : false,
        optimization: {
            minimizer: [
                new UglifyJsPlugin({
                    cache: true,
                    parallel: true,
                    sourceMap: true // set to true if you want JS source maps
                }),
                new OptimizeCSSAssetsPlugin({})
            ]
        },
        module: {
            rules: [
                {
                    test: /\.(scss)$/,
                    use: [{
                        //loader: 'style-loader', // Adiciona o CSS no bundler
                        loader: MiniCssExtractPlugin.loader, options: { publicPath: publicPath } // Cria arquivo CSS separado
                    }, {
                        loader: 'css-loader', // Traduz o CSS para módulo CommonJS
                        options: {
                            //sourceMap: true
                        }
                    }, {
                        loader: 'postcss-loader', // Executa ações do post css
                        options: {
                            plugins: function () {
                                return [
                                    require('precss'),
                                    require('autoprefixer')
                                ];
                            }
                        }
                    }, {
                        loader: 'sass-loader', // Compila o SASS para CSS
                        options: {
                            sourceMap: true
                        }
                    }]
                }
            ]
        },
        plugins: [
            new MiniCssExtractPlugin({
                filename: "[name].css",
                chunkFilename: "[id].css"
            })
        ]
    };
};
